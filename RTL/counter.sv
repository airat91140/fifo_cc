module counter #(
    parameter N = 8
) (
    input                            clock,
    input                            rst,
    input                            en,
    input                            down,
    output logic [$clog2(N) - 1 : 0] q, prev_q, next_q
);

    always @(posedge clock) begin
        if (rst) begin
            q = 0;
            next_q = 1;
            prev_q = N - 1;
        end
        else if (en) begin
            if (!down) begin // up
                prev_q =  q;
                q = next_q;
                next_q = (next_q + 1) != N ? next_q + 1 : 0;
                end
            else begin // down
                next_q = q;
                q = prev_q;
                prev_q = prev_q != 0 ? prev_q - 1 : N - 1;
            end
        end
    end

endmodule : counter
