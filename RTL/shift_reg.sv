module shift_reg #(
    parameter DEPTH = 8,
    parameter WIDTH = 8
) (
    input                          clock,
    input                          rst,
    input  [WIDTH - 1 : 0]         data_i,
    input                          wr_en,
    input                          rd_en,
    input  [$clog2(DEPTH) - 1 : 0] wr_addr,
    output [WIDTH - 1 : 0]         data_o
);

    reg [WIDTH - 1 : 0] mem [DEPTH - 1 : 0];

    always @(posedge clock) begin
        if (rst)
            mem = '{DEPTH{0}};
        else if (wr_en) begin
            mem[wr_addr] = data_i;
        end
        if (rd_en) begin
            for (integer i = 0; i < DEPTH - 1; ++i)
                mem[i] = mem[i+1];
        end
    end

    assign data_o = mem[0];

endmodule : shift_reg
