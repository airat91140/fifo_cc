module syncmem_dp #(
    parameter WIDTH = 8,
    parameter DEPTH = 8
) (
    input  [WIDTH - 1 : 0]         data, // data to write in mem
    input                          wr_en, // enable writing into the mem
    input  [$clog2(DEPTH) - 1 : 0] wr_addr, // address to write
    input  [$clog2(DEPTH) - 1 : 0] rd_addr, // address to read
    input                          clk,
    input                          rst,
    output [WIDTH - 1 : 0]         q   // data at the rd_addr
);

    localparam AD_WIDTH = $clog2(DEPTH);

    reg [WIDTH - 1 : 0] mem [DEPTH - 1 : 0];

    assign q = mem[rd_addr];

    always @(posedge clk) begin
        if (rst)
            mem <= '{DEPTH{0}};
        else if (wr_en)
            mem[wr_addr] <= data;
    end

endmodule : syncmem_dp
