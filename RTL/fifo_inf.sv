interface fifo_inf #(
    parameter P_WIDTH = 8,
    parameter P_DEPTH = 8,
    parameter P_THOLD = 4
) (input  clk_i, rst_n_i);
    logic [P_WIDTH - 1 : 0] data_i;
    logic                   wr_en_i;
    logic                   full_o;
    logic                   afull_o;
    logic [P_WIDTH - 1 : 0] data_o;
    logic                   rd_en_i;
    logic                   empty_o;
    logic                   aempty_o;
    logic                   thold_o;

modport DUT (
    input  data_i,
    input  wr_en_i,
    output full_o,
    output afull_o,
    output data_o,
    input  rd_en_i,
    output empty_o,
    output aempty_o,
    output thold_o,
    input  rst_n_i,
    input  clk_i
);

modport TEST (
    output data_i,
    output wr_en_i,
    input  full_o,
    input  afull_o,
    input  data_o,
    output rd_en_i,
    input  empty_o,
    input  aempty_o,
    input  thold_o,
    input  rst_n_i,
    input  clk_i
);

modport MONITOR (
    input  data_i,
    input  wr_en_i,
    input  full_o,
    input  afull_o,
    input  data_o,
    input  rd_en_i,
    input  empty_o,
    input  aempty_o,
    input  thold_o,
    input  rst_n_i,
    input  clk_i
);
endinterface : fifo_inf
