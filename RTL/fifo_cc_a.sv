module fifo_cc_a #(
    parameter P_WIDTH = 8,
    parameter P_DEPTH = 8,
    parameter P_THOLD = 4
) (
    fifo_inf.DUT inf
);

    logic [$clog2(P_DEPTH) - 1 : 0] rd_ptr, next_rd_ptr, prev_rd_ptr; // reading pointer
    logic [$clog2(P_DEPTH) - 1 : 0] wr_ptr, next_wr_ptr, prev_wr_ptr; // writing pointer
    logic last_op_out;

    syncmem_dp #( // dualport memory
        .WIDTH(P_WIDTH),
        .DEPTH(P_DEPTH)
    ) mem (
        .data(inf.data_i),
        .wr_en(inf.wr_en_i & ~inf.full_o),
        .wr_addr(wr_ptr),
        .rd_addr(rd_ptr),
        .clk(inf.clk_i),
        .rst(~inf.rst_n_i),
        .q(inf.data_o)
    );

    counter #( // counter for reading pointer
        .N(P_DEPTH)
    ) rd_cnt (
        .clock(inf.clk_i),
        .rst(~inf.rst_n_i),
        .en(inf.rd_en_i & ~inf.empty_o),
        .down(1'b0),
        .q(rd_ptr),
        .prev_q(prev_rd_ptr),
        .next_q(next_rd_ptr)
    );

    counter #( // counter for writing pointer
        .N(P_DEPTH)
    ) wr_cnt (
        .clock(inf.clk_i),
        .rst(~inf.rst_n_i),
        .en(inf.wr_en_i & ~inf.full_o),
        .down(1'b0),
        .q(wr_ptr),
        .prev_q(prev_wr_ptr),
        .next_q(next_wr_ptr)
    );

    always @(posedge inf.clk_i) begin // when rd turns into 1, when wr turns into 0, when together rd wins always
        if (!inf.rst_n_i)
            last_op_out <= 1'b1;
        else begin
            last_op_out <= inf.rd_en_i | (last_op_out & ~inf.wr_en_i);
        end
    end

    assign inf.empty_o = (rd_ptr == wr_ptr) & last_op_out;
    assign inf.full_o = (rd_ptr == wr_ptr) & ~last_op_out;
    assign inf.aempty_o = next_rd_ptr == wr_ptr;
    assign inf.afull_o = rd_ptr == next_wr_ptr;
    assign inf.thold_o = wr_ptr > rd_ptr ? (wr_ptr - rd_ptr) >= P_THOLD            :
                         rd_ptr > wr_ptr ? (P_DEPTH - rd_ptr + wr_ptr) >= P_THOLD  :
                         inf.full_o;

endmodule : fifo_cc_a
