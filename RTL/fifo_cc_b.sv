module fifo_cc_b #(
    parameter P_WIDTH = 8,
    parameter P_DEPTH = 8,
    parameter P_THOLD = 4
) (
    fifo_inf.DUT fifo_inf
);

    logic [$clog2(P_DEPTH) - 1: 0] wr_ptr, next_wr_ptr, prev_wr_ptr; // writing pointer
    logic last_op_out;
    
    shift_reg #( // shift register memory
        .WIDTH(P_WIDTH),
        .DEPTH(P_DEPTH)
    ) mem (
        .clock(fifo_inf.clk_i),
        .rst(~fifo_inf.rst_n_i),
        .data_i(fifo_inf.data_i),
        .wr_en(fifo_inf.wr_en_i & ~fifo_inf.full_o),
        .rd_en(fifo_inf.rd_en_i & ~fifo_inf.empty_o),
        .wr_addr(wr_ptr),
        .data_o(fifo_inf.data_o)
    );
     
    counter #( // counter for writing pointer
        .N(P_DEPTH)
    ) cnt (
        .clock(fifo_inf.clk_i),
        .rst(~fifo_inf.rst_n_i),
        .en((fifo_inf.rd_en_i & ~fifo_inf.empty_o) ^ (fifo_inf.wr_en_i & ~fifo_inf.full_o)),
        .down((fifo_inf.rd_en_i & ~fifo_inf.empty_o) & ~(fifo_inf.wr_en_i & ~fifo_inf.full_o)),
        .q(wr_ptr),
        .prev_q(prev_wr_ptr), 
        .next_q(next_wr_ptr)
    );
     
    always @(posedge inf.clk_i) begin // when rd turns into 1, when wr turns into 0, when together rd wins always
     	if (!inf.rst_n_i)
     		last_op_out <= 1'b1;
     	else begin
     		last_op_out <= inf.rd_en_i | (last_op_out & ~inf.wr_en_i);
     	end
    end     
     
    assign fifo_inf.empty_o = (~|wr_ptr) & last_op_out;
    assign fifo_inf.full_o = (~|wr_ptr) & ~last_op_out;
    assign fifo_inf.aempty_o = ~|prev_wr_ptr;
    assign fifo_inf.afull_o = ~|next_wr_ptr;
    assign fifo_inf.thold_o = (wr_ptr >= (P_THOLD)) | fifo_inf.full_o;
     
endmodule : fifo_cc_b
