/****************************************************************************
 * fifo_monitor.sv
 ****************************************************************************/
`ifndef FIFO_MONITOR__SV
`define FIFO_MONITOR__SV

`include "simple_item.sv"
`include "fifo_transfer.sv"

/**
 * Class: fifo_monitor
 *
 * monitor class for accessing outputs of the DUT
 */
class fifo_monitor #(
    parameter P_WIDTH = 8,
    parameter P_THOLD = 4,
    parameter P_DEPTH = 8
) extends uvm_monitor;
    
    fifo_transfer#(P_WIDTH) trans;

    virtual fifo_inf #(
        .P_DEPTH(P_DEPTH),
        .P_THOLD(P_THOLD),
        .P_WIDTH(P_WIDTH)
    ).MONITOR vif;

    fifo_agent_cfg agt_cfg;

    uvm_analysis_port #(fifo_transfer#(P_WIDTH)) item_collected_port;

    `uvm_component_param_utils(fifo_cc_tb_pkg::fifo_monitor#(.P_WIDTH(P_WIDTH), .P_DEPTH(P_DEPTH), .P_THOLD(P_THOLD)))
        
    covergroup cg;
        full: coverpoint {trans.read, trans.write, trans.full};
        empty: coverpoint {trans.read, trans.write, trans.empty};
        afull: coverpoint {trans.read, trans.write, trans.afull};
        aempty: coverpoint {trans.read, trans.write, trans.aempty};
        thold: coverpoint {trans.read, trans.write, trans.thold};
    endgroup

    virtual function void set_config(fifo_agent_cfg agt_cfg);
        this.agt_cfg = agt_cfg;
    endfunction : set_config

    function new(string name, uvm_component parent);
        super.new(name, parent);
        item_collected_port = new("item_collected_port", this);
        cg = new();
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        if(!uvm_config_db#(virtual fifo_inf #(.P_DEPTH(P_DEPTH),
                                              .P_THOLD(P_THOLD),
                                              .P_WIDTH(P_WIDTH)))::get(this, "", "vif_monitor", vif))
            `uvm_fatal("NOVIF", {"virtual interface must be set for: ", get_full_name(),".vif_monitor"});            
    endfunction : build_phase

    virtual task run_phase(uvm_phase phase);
        collect_transactions();
    endtask : run_phase

    virtual protected task collect_transactions();
        @(posedge vif.rst_n_i);
        forever begin
            @(posedge vif.clk_i);
            if (!vif.wr_en_i && !vif.rd_en_i && !agt_cfg.coverage_on)
                continue;
            trans = fifo_transfer#(P_WIDTH)::type_id::create("fifo_transfer");
            write_to_trans(trans);
            if (agt_cfg.coverage_on)
                perform_transfer_coverage();
            if (!vif.wr_en_i && !vif.rd_en_i )
                continue;
            item_collected_port.write(trans);
            if (agt_cfg.monitor_log) begin
                `uvm_info("trans sent",
                    $sformatf("WR: %b, RD: %b \nfull: %b, afull: %b, empty: %b, aempty: %b \ndata_i: %b, data_o: %b",
                        trans.write, trans.read, trans.full, trans.afull, trans.empty, trans.aempty, trans.data_i, trans.data_o), UVM_MEDIUM)
            end
        end
    endtask : collect_transactions

    function void write_to_trans(ref fifo_transfer#(P_WIDTH) trans);
        trans.write  = vif.wr_en_i;
        trans.read   = vif.rd_en_i;
        trans.data_i = vif.data_i;
        trans.data_o = vif.data_o;
        trans.empty  = vif.empty_o;
        trans.aempty = vif.aempty_o;
        trans.full   = vif.full_o;
        trans.afull  = vif.afull_o;
        trans.thold  = vif.thold_o;
    endfunction : write_to_trans

    virtual protected function void perform_transfer_coverage();
        cg.sample();
    endfunction : perform_transfer_coverage

endclass : fifo_monitor

`endif // FIFO_MONITOR__SV
