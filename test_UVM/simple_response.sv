/****************************************************************************
 * simple_response.sv
 ****************************************************************************/
`ifndef SIMPLE_RESPONSE__SV
`define SIMPLE_RESPONSE__SV

/**
 * Class: simple_response
 *
 * class that provides response  from the DUT to sequencer
 */
class simple_response extends uvm_sequence_item;
    bit read_success, write_success;
    
    `uvm_object_utils(fifo_cc_tb_pkg::simple_response)

    function new(string name = "simple_response");
        super.new(name);
    endfunction : new

endclass : simple_response

`endif // SIMPLE_RESPONSE__SV
