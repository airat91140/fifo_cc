/****************************************************************************
 * fifo_agent.sv
 ****************************************************************************/
`ifndef FIFO_AGENT__SV
`define FIFO_AGENT__SV

`include "fifo_driver.sv"
`include "fifo_monitor.sv"
`include "simple_item.sv"
`include "simple_response.sv"
`include "fifo_agent_cfg.sv"


/**
 * Class: fifo_agent
 *
 * class for implementing agent functions
 */
class fifo_agent #(
    parameter P_WIDTH = 8,
    parameter P_DEPTH = 8,
    parameter P_THOLD = 4
) extends uvm_agent;

    fifo_agent_cfg cfg;

    `uvm_component_param_utils(fifo_cc_tb_pkg::fifo_agent#(P_WIDTH, P_DEPTH, P_THOLD))

    function new(string name = "fifo_agent", uvm_component parent);
        super.new(name, parent);
    endfunction : new

    uvm_sequencer #(simple_item#(P_WIDTH), simple_response) sequencer;

    fifo_driver #(
        .P_DEPTH(P_DEPTH),
        .P_THOLD(P_THOLD),
        .P_WIDTH(P_WIDTH)
    ) driver;

    fifo_monitor #(
        .P_DEPTH(P_DEPTH),
        .P_THOLD(P_THOLD),
        .P_WIDTH(P_WIDTH)
    ) monitor;

    virtual function void set_config(fifo_agent_cfg cfg);
        this.cfg = cfg;
    endfunction : set_config

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        monitor = fifo_monitor#(.P_DEPTH(P_DEPTH), .P_THOLD(P_THOLD), .P_WIDTH(P_WIDTH))::type_id::create("monitor", this);
        monitor.set_config(cfg);
        if (cfg.active == UVM_ACTIVE) begin
            sequencer = uvm_sequencer#(simple_item#(P_WIDTH), simple_response)::type_id::create("sequencer", this);
            driver = fifo_driver#(.P_DEPTH(P_DEPTH), .P_THOLD(P_THOLD), .P_WIDTH(P_WIDTH))::type_id::create("driver", this);
            driver.set_config(cfg);
        end
    endfunction : build_phase

    virtual function void connect_phase(uvm_phase phase);
        if (cfg.active == UVM_ACTIVE)
            driver.seq_item_port.connect(sequencer.seq_item_export);
    endfunction : connect_phase

endclass : fifo_agent

`endif  // FIFO_AGENT__SV
