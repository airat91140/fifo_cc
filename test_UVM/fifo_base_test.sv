/****************************************************************************
 * fifo_base_test.sv
 ****************************************************************************/
`ifndef FIFO_BASE_TEST__SV
`define FIFO_BASE_TEST__SV

`include "fifo_env.sv"
`include "fifo_env_cfg.sv"

/**
 * Class: fifo_base_test
 *
 * base class for all tests
 */
class fifo_base_test extends uvm_test;
    parameter P_WIDTH = `P_WIDTH;
    parameter P_DEPTH = `P_DEPTH;
    parameter P_THOLD = `P_THOLD;

    fifo_env_cfg env_cfg;

    fifo_env #(
        .P_DEPTH(P_DEPTH),
        .P_THOLD(P_THOLD),
        .P_WIDTH(P_WIDTH)
    ) env;

    uvm_sequence #(simple_item#(P_WIDTH), simple_response) seq;

    uvm_table_printer printer;
    bit test_pass = 1;

    `uvm_component_utils(fifo_base_test)

    function new(string name = "fifo_base_test", uvm_component parent = null);
        super.new(name, parent);
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        uvm_config_db#(int)::set(this, "*", "recording_detail", UVM_FULL);
        env = fifo_env #(
            .P_DEPTH(P_DEPTH),
            .P_THOLD(P_THOLD),
            .P_WIDTH(P_WIDTH)
        )::type_id::create("env", this);
        env_cfg = fifo_env_cfg::type_id::create("fifo_env_cfg");
        env.set_config(env_cfg);
        seq.randomize();
        printer = new();
        printer.knobs.depth = 3;
    endfunction

    function void extract_phase(uvm_phase phase);
        if(env.scb.sbd_error)
            test_pass = 1'b0;
    endfunction	: extract_phase

    function void report_phase(uvm_phase phase);
        if(test_pass)
            `uvm_info(get_type_name(), "** UVM TEST PASSED **", UVM_NONE)
        else
            `uvm_error(get_type_name(), "** UVM TEST FAIL **")
    endfunction	:report_phase

    task run_phase(uvm_phase phase);
        phase.raise_objection(this);
        seq.start(env.agent.sequencer);
        phase.drop_objection(this);
    endtask : run_phase

endclass : fifo_base_test

`endif  // FIFO_BASE_TEST__SV
