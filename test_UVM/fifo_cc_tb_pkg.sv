`ifndef FIFO_CC_TB_PKG__SV
`define FIFO_CC_TB_PKG__SV

package fifo_cc_tb_pkg;

import uvm_pkg::*;
`include "uvm_macros.svh"

`include "fifo_agent_cfg.sv"
`include "fifo_env_cfg.sv"
`include "fifo_env.sv"
`include "fifo_seq0.sv"
`include "fifo_seq1.sv"

endpackage : fifo_cc_tb_pkg

`endif // FIFO_CC_TB_PKG__SV