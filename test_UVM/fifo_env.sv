/****************************************************************************
 * fifo_env.sv
 ****************************************************************************/
`ifndef FIFO_ENV__SV
`define FIFO_ENV__SV

`include "fifo_agent.sv"
`include "fifo_scoreboard.sv"

/**
 * Class: fifo_env
 *
 * top environment class
 */
class fifo_env #(
    parameter P_WIDTH = 8,
    parameter P_DEPTH = 8,
    parameter P_THOLD = 4
) extends uvm_env;

    fifo_env_cfg cfg;

    fifo_agent #(
        .P_WIDTH(P_WIDTH),
        .P_DEPTH(P_DEPTH),
        .P_THOLD(P_THOLD)
    ) agent;

    fifo_scoreboard #(
        .P_WIDTH(P_WIDTH),
        .P_DEPTH(P_DEPTH),
        .P_THOLD(P_THOLD)
    ) scb;

    `uvm_component_param_utils(fifo_cc_tb_pkg::fifo_env#(.P_WIDTH(P_WIDTH), .P_DEPTH(P_DEPTH), .P_THOLD(P_THOLD)))

    function new(string name = "fifo_env", uvm_component parent);
        super.new(name, parent);
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        agent = fifo_agent#(.P_WIDTH(P_WIDTH), .P_DEPTH(P_DEPTH), .P_THOLD(P_THOLD))::type_id::create("agent", this);
        agent.set_config(cfg.agt_cfg);
        scb = fifo_scoreboard#(.P_WIDTH(P_WIDTH), .P_DEPTH(P_DEPTH), .P_THOLD(P_THOLD))::type_id::create("scoreboard", this);
        scb.set_config(cfg);
    endfunction : build_phase

    virtual function void connect_phase(uvm_phase phase);
        agent.monitor.item_collected_port.connect(scb.item_collected_export);
    endfunction : connect_phase

    virtual function void set_config(fifo_env_cfg cfg);
        this.cfg = cfg;
    endfunction : set_config

endclass : fifo_env

`endif // FIFO_ENV__SV
