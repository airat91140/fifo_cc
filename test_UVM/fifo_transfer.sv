/****************************************************************************
 * fifo_transfer.sv
 ****************************************************************************/
`ifndef FIFO_TRANSFER__SV
`define FIFO_TRANSFER__SV

/**
 * Class: fifo_transfer
 *
 * packet that goes from DUT to scoreboard
 */
class fifo_transfer #(parameter P_WIDTH = 8) extends uvm_object;
    bit write, read;
    bit full, afull, empty, aempty, thold;
    bit [P_WIDTH - 1 : 0] data_i, data_o;

    `uvm_object_param_utils(fifo_cc_tb_pkg::fifo_transfer#(P_WIDTH));

    function new(string name = "fifo_transfer");
        super.new(name);
    endfunction

endclass

`endif // FIFO_TRANSFER__SV
