/****************************************************************************
 * fifo_scoreboard.sv
 ****************************************************************************/
`ifndef FIFO_SCOREBOARD__SV
`define FIFO_SCOREBOARD__SV

`include "fifo_transfer.sv"

/**
 * Class: fifo_scoreboard
 *
 * class for scoreboard
 */
class fifo_scoreboard #(
    parameter P_WIDTH = 8,
    parameter P_THOLD = 4,
    parameter P_DEPTH = 8
) extends uvm_scoreboard;

    bit [P_WIDTH - 1 : 0] exp_fifo [$ : P_DEPTH - 1];
    int sbd_error = 0;
    fifo_env_cfg env_cfg;

    uvm_analysis_imp #(
        fifo_transfer#(P_WIDTH),
        fifo_scoreboard#(
            .P_WIDTH(P_WIDTH),
            .P_DEPTH(P_DEPTH),
            .P_THOLD(P_THOLD)
        )
    ) item_collected_export;

    `uvm_component_param_utils(fifo_cc_tb_pkg::fifo_scoreboard#(.P_WIDTH(P_WIDTH), .P_DEPTH(P_DEPTH), .P_THOLD(P_THOLD)))

    virtual function void set_config(fifo_env_cfg env_cfg);
        this.env_cfg = env_cfg;
    endfunction : set_config

    function new(string name = "fifo_scoreboard", uvm_component parent = null);
        super.new(name, parent);
    endfunction : new

    function void build_phase(uvm_phase phase);
        item_collected_export = new("item_collected_export", this);
    endfunction	: build_phase

    virtual function void write(fifo_transfer#(P_WIDTH) trans);
        if (env_cfg.scoreboard_log) begin
            `uvm_info("trans recieved",
                $sformatf("WR: %b, RD: %b \nfull: %b, afull: %b, empty: %b, aempty: %b \ndata_i: %b, data_o: %b",
                    trans.write, trans.read, trans.full, trans.afull, trans.empty, trans.aempty, trans.data_i, trans.data_o), UVM_MEDIUM)
        end
        check_flags(trans);
        check_data(trans);
        write_data(trans);

    endfunction : write

    function void check_flags(fifo_transfer trans);
        if ((exp_fifo.size() == 0) != trans.empty) begin
            sbd_error++;
            `uvm_error("FLGERR", $sformatf("incorrect empty flag: expected %b, recieved %b", (exp_fifo.size() == 0), trans.empty));
        end
        if ((exp_fifo.size() == 1) != trans.aempty) begin
            sbd_error++;
            `uvm_error("FLGERR", $sformatf("incorrect aempty flag: expected %b, recieved %b", (exp_fifo.size() == 1), trans.aempty));
        end
        if ((exp_fifo.size() == P_DEPTH) != trans.full) begin
            sbd_error++;
            `uvm_error("FLGERR", $sformatf("incorrect full flag: expected %b, recieved %b", (exp_fifo.size() == P_DEPTH), trans.full));
        end
        if ((exp_fifo.size() == P_DEPTH - 1) != trans.afull) begin
            sbd_error++;
            `uvm_error("FLGERR", $sformatf("incorrect afull flag: expected %b, recieved %b", (exp_fifo.size() == P_DEPTH - 1), trans.afull));
        end
        if ((exp_fifo.size() >= P_THOLD) != trans.thold) begin
            sbd_error++;
            `uvm_error("FLGERR", $sformatf("incorrect thold flag: expected %b, recieved %b", (exp_fifo.size() >= P_THOLD), trans.thold));
        end
    endfunction : check_flags

    function void check_data(fifo_transfer#(P_WIDTH) trans);
        if (exp_fifo.size() != 0) begin
            if (exp_fifo[0] != trans.data_o) begin
                sbd_error++;
                `uvm_error("DATERR", $sformatf("incorrect data output: expected %b, recieved %b", exp_fifo[0], trans.data_o));
            end
        end
    endfunction : check_data

    function void write_data(fifo_transfer#(P_WIDTH) trans);
        if (trans.write & ~trans.read & exp_fifo.size != P_DEPTH)
            exp_fifo.push_back(trans.data_i);
        if (trans.read & ~trans.write)
            void'(exp_fifo.pop_front());
        if (trans.read & trans.write) begin
            if (trans.empty)
                exp_fifo.push_back(trans.data_i);
            else if (trans.full)
                void'(exp_fifo.pop_front());
            else begin
                exp_fifo.push_back(trans.data_i);
                void'(exp_fifo.pop_front());
            end
        end
    endfunction : write_data

endclass : fifo_scoreboard

`endif // FIFO_SCOREBOARD__SV
