`ifndef FIFO_TOP__SV
`define FIFO_TOP__SV

`timescale 1ns/1ns
`define P_WIDTH 8
`define P_DEPTH 8
`define P_THOLD 4

import uvm_pkg::*;
`include "uvm_macros.svh"

`include "fifo_cc_tb_pkg.sv"
import fifo_cc_tb_pkg::*;

`include "test_lib.sv"

module top;
    parameter P_DEPTH = `P_DEPTH;
    parameter P_THOLD = `P_THOLD;
    parameter P_WIDTH = `P_WIDTH;

    logic clk;
    logic rst;

    fifo_inf #(
        .P_DEPTH(P_DEPTH),
        .P_THOLD(P_THOLD),
        .P_WIDTH(P_WIDTH)
    ) inf (clk, rst);

    virtual fifo_inf #(
        .P_DEPTH(P_DEPTH),
        .P_THOLD(P_THOLD),
        .P_WIDTH(P_WIDTH)
    ) vif;

    initial begin
        clk = 0;
        forever
            #5ns clk = ~clk;
    end

    initial begin
        rst = 1;
        #5ns rst = 0;
        @(posedge clk);
        rst = 1;
    end

    initial begin
        uvm_root root;
        vif = inf;
        root = uvm_root::get();
        uvm_config_db#(virtual fifo_inf#(.P_DEPTH(P_DEPTH), .P_THOLD(P_THOLD), .P_WIDTH(P_WIDTH)))::set(root, "*", "vif_test", vif);
        uvm_config_db#(virtual fifo_inf#(.P_DEPTH(P_DEPTH), .P_THOLD(P_THOLD), .P_WIDTH(P_WIDTH)))::set(root, "*", "vif_monitor", vif);
        run_test();
    end

    fifo_cc_b #(
        .P_DEPTH(P_DEPTH),
        .P_THOLD(P_THOLD),
        .P_WIDTH(P_WIDTH)
    ) i_fifo_cc (inf.DUT);

endmodule : top

`endif // FIFO_TOP__SV
