/****************************************************************************
 * simple_item.sv
 ****************************************************************************/
`ifndef SIMPLE_ITEM__SV
`define SIMPLE_ITEM__SV



/**
 * Class: simple_item
 *
 * class that provides implementation of transaction from sequencer to driver
 */
class simple_item #(parameter P_WIDTH = 8) extends uvm_sequence_item;
    rand bit is_write, is_read;
    rand bit [P_WIDTH - 1 : 0] data;
    rand bit is_noaction;

    `uvm_object_param_utils(fifo_cc_tb_pkg::simple_item#(P_WIDTH))

    constraint one_operation_c {
        ~is_noaction -> is_write != is_read;
        solve is_noaction before is_write;
        solve is_noaction before is_read;
    }

    constraint operation_distribution_c {
        is_noaction dist {0:=70, 1:=30};
        is_write    dist {0:=50, 1:=50};
        is_read     dist {0:=50, 1:=50};
    }

    function new(string name = "simple_item");
        super.new(name);
    endfunction : new

endclass : simple_item

`endif // SIMPLE_ITEM__SV
