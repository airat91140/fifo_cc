/****************************************************************************
 * fifo_seq1.sv
 ****************************************************************************/
`ifndef FIFO_SEQ1__SV
`define FIFO_SEQ1__SV
    
`include "simple_item.sv"
`include "simple_response.sv"

/**
 * Class: fifo_seq1
 * 
 * sequence class with several operations per cycle and writing and reading to empty and full
 */
class fifo_seq1 #(parameter P_WIDTH = 8, parameter P_DEPTH = 8) extends uvm_sequence #(simple_item#(P_WIDTH), simple_response);
    rand integer count;
    simple_item #(P_WIDTH) req;
    simple_response rsp;
    fifo_agent_cfg agt_cfg;
    
    constraint count_c {
        count inside {[10000 : 50000]};
    }
    
    function new(string name = "fifo_seq");
        super.new(name);
    endfunction : new
    
    `uvm_object_param_utils(fifo_cc_tb_pkg::fifo_seq1#(P_WIDTH, P_DEPTH))

    virtual function void set_config(fifo_agent_cfg agt_cfg);
        this.agt_cfg = agt_cfg;
    endfunction : set_config

    virtual task body();
        repeat (count) begin
            req = simple_item#(P_WIDTH)::type_id::create("item");
            req.one_operation_c.constraint_mode(0);
            wait_for_grant();                         
            assert(req.randomize());  
            send_request(req);
            if (agt_cfg.sequence_log) begin
                `uvm_info("item sent",
                    $sformatf("WR: %b, RD: %b, no_action: %b \ndata: %b",
                        req.is_write, req.is_read, req.is_noaction, req.data), UVM_MEDIUM)
            end
            wait_for_item_done();                       
            get_response(rsp);
            if (agt_cfg.sequence_log) begin
                `uvm_info("response recieved",
                    $sformatf("write_success: %b, read_success: %b", rsp.write_success, rsp.read_success), UVM_MEDIUM)
            end
        end
    endtask : body
    
endclass : fifo_seq1

`endif // FIFO_SEQ1__SV
