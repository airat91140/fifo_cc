/****************************************************************************
 * fifo_driver.sv
 ****************************************************************************/
`ifndef FIFO_DRIVER__SV
`define FIFO_DRIVER__SV

`include "simple_item.sv"
`include "simple_response.sv"

/**
 * Class: fifo_driver
 *
 * driver class
 */
class fifo_driver #(
    parameter P_WIDTH = 8,
    parameter P_DEPTH = 8,
    parameter P_THOLD = 4
) extends uvm_driver #(simple_item#(P_WIDTH), simple_response);

    simple_item #(P_WIDTH) item;
    simple_response response;
    fifo_agent_cfg agt_cfg;

    virtual fifo_inf #(
        .P_DEPTH(P_DEPTH),
        .P_THOLD(P_THOLD),
        .P_WIDTH(P_WIDTH)
    ).TEST vif;

    `uvm_component_param_utils(fifo_cc_tb_pkg::fifo_driver#(P_WIDTH, P_DEPTH, P_THOLD))

    virtual function void set_config(fifo_agent_cfg agt_cfg);
        this.agt_cfg = agt_cfg;
    endfunction : set_config

    function new(string name = "fifo driver", uvm_component parent);
        super.new(name, parent);
    endfunction : new

    function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        if(!uvm_config_db#(virtual fifo_inf #(.P_DEPTH(P_DEPTH),
                                              .P_THOLD(P_THOLD),
                                              .P_WIDTH(P_WIDTH)))::get(this, "", "vif_test", vif))
            `uvm_fatal("NOVIF", {"virtual interface must be set for: ", get_full_name(),".vif_test"});
    endfunction : build_phase

    task run_phase(uvm_phase phase);
        bit [P_WIDTH - 1 : 0] tmp;
        vif.wr_en_i = 1'b0;
        vif.rd_en_i = 1'b0;
        vif.data_i = '0;
        @(posedge vif.rst_n_i);
        forever begin
            @(posedge vif.clk_i);
            seq_item_port.get_next_item(item);
            drive_item(item);
            if (agt_cfg.driver_log) begin
                `uvm_info("item is driven",
                    $sformatf("wr_en_i: %b, rd_en_i: %b \nfull_o: %b, afull_o: %b, empty_o: %b, aempty_o: %b \ndata_i: %b, data_o: %b",
                vif.wr_en_i,vif.rd_en_i, vif.full_o, vif.afull_o, vif.empty_o, vif.aempty_o, vif.data_i, vif.data_o), UVM_MEDIUM)
            end
            response = simple_response::type_id::create("response");
            response.read_success = vif.rd_en_i ? !vif.empty_o : 1'b0;
            response.write_success = vif.wr_en_i ? !vif.full_o : 1'b0;
            response.set_sequence_id(item.get_sequence_id());
            seq_item_port.item_done(response);
            if (agt_cfg.driver_log) begin
                `uvm_info("response is sent",
                    $sformatf("read_success: %b, write_success: %b", response.read_success, response.write_success), UVM_MEDIUM)
            end
        end
    endtask : run_phase

    task drive_item(simple_item#(P_WIDTH) item);
        bit wr_en, rd_en;
        vif.data_i = item.data;
        wr_en = 1'b0;
        rd_en = 1'b0;
        if (item.is_noaction) begin
            wr_en = 1'b0;
            rd_en = 1'b0;
        end else begin
            if (item.is_write)
                wr_en = 1'b1;
            if (item.is_read)
                rd_en = 1'b1;
        end
        vif.wr_en_i = wr_en;
        vif.rd_en_i = rd_en;
    endtask : drive_item

endclass : fifo_driver

`endif // FIFO_DRIVER__SV
