`ifndef FIFO_AGENT_CFG__SV
`define FIFO_AGENT_CFG__SV

class fifo_agent_cfg extends uvm_object;

    `uvm_object_utils(fifo_cc_tb_pkg::fifo_agent_cfg)

    uvm_active_passive_enum active;
    bit driver_log;
    bit monitor_log;
    bit sequence_log;
    bit coverage_on;

    function new(string name = "fifo_agent_cfg");
        super.new(name);
        active = UVM_ACTIVE;
        driver_log = 0;
        monitor_log = 0;
        sequence_log = 0;
        coverage_on = 0;
    endfunction : new
endclass : fifo_agent_cfg

`endif // FIFO_AGENT_CFG__SV
