/****************************************************************************
 * fifo_seq0.sv
 ****************************************************************************/
`ifndef FIFO_SEQ0__SV
`define FIFO_SEQ0__SV

`include "simple_item.sv"
`include "simple_response.sv"

/**
 * Class: fifo_seq0
 *
 * sequence class with only one operation per cycle, no writing to full fifo and no reading from empty
 */
class fifo_seq0 #(parameter P_WIDTH = 8, parameter P_DEPTH = 8) extends uvm_sequence #(simple_item#(P_WIDTH), simple_response);
    rand integer count;
    integer q_size;
    simple_item #(P_WIDTH) req;
    simple_response rsp;
    fifo_agent_cfg agt_cfg;

    constraint count_c {
        count inside {[100 : 500]};
    }

    function new(string name = "fifo_seq0");
        super.new(name);
        q_size = 0;
    endfunction : new

    `uvm_object_param_utils(fifo_cc_tb_pkg::fifo_seq0#(P_WIDTH, P_DEPTH))

    virtual function void set_config(fifo_agent_cfg agt_cfg);
        this.agt_cfg = agt_cfg;
    endfunction : set_config

    virtual task body();
        bit read_forbidden = 1, write_forbidden = 0;
        repeat (count) begin
            req = simple_item#(P_WIDTH)::type_id::create("item");
            wait_for_grant();
            assert(req.randomize());
            if (!req.is_noaction) begin
                if (req.is_read & read_forbidden) begin // this transaction wants to read but it is forbidden
                    if (agt_cfg.sequence_log) begin
                        `uvm_info("item_generated", "generated item is trying to read from empty fifo. reverting operations", UVM_MEDIUM)
                    end
                    req.is_read = 0;
                    req.is_write = 1;
                end
                else if (req.is_write & write_forbidden) begin // this transaction wants to write but it is forbidden
                    if (agt_cfg.sequence_log) begin
                        `uvm_info("item generated", "generated item is trying to read from empty fifo. reverting operations", UVM_MEDIUM)
                    end
                    req.is_read = 1;
                    req.is_write = 0;
                end
            end
            send_request(req);
            if (agt_cfg.sequence_log) begin
                `uvm_info("item sent ",
                    $sformatf("WR: %b, RD: %b, no_action: %b \ndata: %b",
                        req.is_write, req.is_read, req.is_noaction, req.data), UVM_MEDIUM)
            end
            wait_for_item_done();
            get_response(rsp);
            if (agt_cfg.sequence_log) begin
                `uvm_info("response recieved ",
                    $sformatf("write_success: %b, read_success: %b", rsp.write_success, rsp.read_success), UVM_MEDIUM)
            end
            q_size += rsp.write_success;
            q_size -= rsp.read_success; 
            read_forbidden = q_size <= 0;
            write_forbidden = q_size >= P_DEPTH;
        end
    endtask : body

endclass : fifo_seq0

`endif // FIFO_SEQ0__SV
