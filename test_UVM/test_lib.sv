/****************************************************************************
 * test_lib.sv
 ****************************************************************************/
`ifndef TEST_LIB__SV
`define TEST_LIB__SV

import uvm_pkg::*;
`include "uvm_macros.svh"

`include "fifo_base_test.sv"
`include "fifo_seq0.sv"

/**
 * Class: one_operation_without_overflow_test
 *
 * test that provides test with only one operation per cycle and without testing writing to full and reading from empty
 */
class one_operation_without_overflow_test extends fifo_base_test;
    `uvm_component_utils(one_operation_without_overflow_test)

    function new(string name = "one_operation_without_overflow_test", uvm_component parent = null);
        super.new(name, parent);
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        fifo_seq0#(super.P_WIDTH) tmp;
        tmp = fifo_seq0#(super.P_WIDTH)::type_id::create("seq");
        super.seq = tmp;
        super.build_phase(phase);
        tmp.set_config(super.env_cfg.agt_cfg);

    endfunction : build_phase

endclass : one_operation_without_overflow_test


`include "fifo_seq1.sv"

/**
 * Class: multiple_operations_with_overflow_test
 *
 * test with multiple operations per one cycle with testing the possibility to write into full or read from empty
 */
class multiple_operations_with_overflow_test extends fifo_base_test;
    `uvm_component_utils(multiple_operations_with_overflow_test)

    function new(string name = "multiple_operations_with_overflow_test", uvm_component parent = null);
        super.new(name, parent);
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        fifo_seq1#(super.P_WIDTH) tmp;
        tmp = fifo_seq1#(super.P_WIDTH)::type_id::create("seq");
        super.seq = tmp;
        super.build_phase(phase);
        tmp.set_config(super.env_cfg.agt_cfg);
    endfunction : build_phase

endclass : multiple_operations_with_overflow_test

`endif  //  TEST_LIB__SV
