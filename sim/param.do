#############################################
## set home directory for the project:
#############################################

set proj_home "/media/sf_uvmtut/Untitled Folder/lab1"

#############################################
## Enable simulation optimization (0/1):
#############################################

set opt 1

#############################################
## Set verbosity level:
## (NONE, LOW, MEDIUM, HIGH, FULL, DEBUG)
##  min.output                max.output
#############################################

#set verbosity LOW
set verbosity MEDIUM

#############################################
## Set simulation time scale:
#############################################

set ts_param "1ns/10ps"

############################################# 
## Store debug database (0/1):
#############################################

set debug_db 1

#############################################
## Set test name:
#set multiple_operations_with_overflow_test
#set one_operation_without_overflow_test
#############################################
 
set test_name multiple_operations_with_overflow_test

#############################################
## Set random seed (0 - auto):
#############################################

set debugseed   0

#############################################
## Set code_coverage (0 - 1):
#############################################

set code_coverage 1

#############################################
## Set path to all test components:
#############################################

set rtl_dir                $proj_home/RTL

set tb_dir                 $proj_home/test_UVM

#############################################
## End of configuration
#############################################
