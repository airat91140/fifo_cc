############################################# 
## Create functional coverage report
#############################################
coverage report -detail -cvg -comments -file fcover_report.txt

############################################# 
## Show functional coverage report
#############################################
notepad fcover_report.txt

