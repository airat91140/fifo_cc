quit -sim

#clean work
if {[file exists "work"]} {
    vdel -lib work -all
}

#clean fake_dut
if {[file exists "fake_dut"]} {
    vdel -lib fake_dut -all
}

#clean single test .html log files
foreach file_to_del [glob -nocomplain *_log.html] {file delete $file_to_del}

#clean transcript .log file
transcript file ""
foreach file_to_del [glob -nocomplain *.log] {file delete $file_to_del}
file delete "transcript"

#clean .bak files
foreach file_to_del [glob -nocomplain *.bak] {file delete $file_to_del}
foreach file_to_del [glob -nocomplain ../rtl/*.bak] {file delete $file_to_del}
foreach file_to_del [glob -nocomplain ../vip/*.bak] {file delete $file_to_del}

#clean .txt files
foreach file_to_del [glob -nocomplain *.txt] {file delete $file_to_del}

#clean .ucdb files
foreach file_to_del [glob -nocomplain *.ucdb] {file delete $file_to_del}
foreach file_to_del [glob -nocomplain ../ucdb/*.ucdb] {file delete $file_to_del}

#clean .dbg files
foreach file_to_del [glob -nocomplain *.dbg] {file delete $file_to_del}

#clean .wlf files
foreach file_to_del [glob -nocomplain *.wlf] {file delete $file_to_del}

