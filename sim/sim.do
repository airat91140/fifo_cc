source param.do

if {$debugseed} {
    quietly set seed $debugseed
} else {
    quietly set seed [clock seconds]
}

if {$opt} {
    quietly set opt_param "-voptargs=\"+acc\""
} else {
    quietly set opt_param "-novopt"
}

if {$debug_db} {
    quietly set dbg "-debugDB"
} else {
    quietly set dbg ""
}

if {$code_coverage} {
    quietly set cov "-coverage"
} else {
    quietly set cov ""
}

quietly set verb_level "+UVM_VERBOSITY=UVM_$verbosity"
if {$verbosity == "LOW"} {
    quietly set set_verb "+uvm_set_verbosity=*,TEST_DONE,UVM_NONE,run"
} else {
    quietly set set_verb ""
}
quietly set set_act "+uvm_set_action=*,RNTST,_ALL_,UVM_LOG|UVM_DISPLAY"

vsim $opt_param $cov $verb_level $set_verb $set_act $dbg +UVM_TESTNAME=$test_name -L unisims_ver -L mtiUvm +nowarnTSCALE -sv_seed $seed top   

transcript file ""
transcript file tb.log
