coverage save -directive -cvg -codeAll ../ucdb/tmp.ucdb

############################################# 
## Merge all coverage data to total
#############################################
vcover merge ../ucdb/total.ucdb ../ucdb/*.ucdb

############################################# 
## Show total coverage report
#############################################
coverage open ../ucdb/total.ucdb

