# FIFO с общим тактированием (lab1)

## Структура проекта:
* [RTL](./RTL) - RTL-код проекта
    * [counter.sv](./RTL/counter.sv) - реверсивный счетчик
    * [fifo_cc_a.sv](./RTL/fifo_cc_a.sv) - модуль верхнего уровня для пункта а \(на основе двухпортовой памяти\)
    * [fifo_cc_b.sv](./RTL/fifo_cc_b.sv) - модуль верхнего уровня для пункта б \(на основе сдвиговых регистров\)
    * [fifo_inf.sv](./RTL/fifo_inf.sv) - интерфейс для FIFO
    * [shift_reg.sv](./RTL/shift_reg.sv) - сдвиговый регистр
    * [syncmem_dp.sv](./RTL/syncmem_dp.sv) - двухпортовая память
* [sim](./sim) - скрипты для симуляции
    * [clean.do](./sim/clean.do) - очистка файлов компиляции
    * [compile.do](./sim/compile.do) - компиляции файлов проекта
    * [param.do](./sim/param.do) - параметры компиляции
    * [rerun.do](./sim/rerun.do) - компиляции и запуск симулятора
    * [sim.do](./sim/sim.do) - запуск симуляции проекта
    * [wave.do](./sim/wave.do) - открытие временной диаграммы FIFO
* [test_UVM](./test_UVM) - тестировние проекта с использованием UVM
    * [fifo_agent.sv](./test_UVM/fifo_agent.sv) - класс агента
    * [fifo_driver.sv](./test_UVM/fifo_driver.sv) - класс драйвера
    * [fifo_env.sv](./test_UVM/fifo_env.sv) - класс верхнего environment
    * [fifo_monitor.sv](./test_UVM/fifo_monitor.sv) - класс монитора
    * [fifo_scoreboard.sv](./test_UVM/fifo_scoreboard.sv) - класс scoreboard
    * [fifo_base_test.sv](./test_UVM/fifo_base_test.sv) - базовый класс для тестов (расширение uvm_test)
    * [fifo_seq0.sv](./test_UVM/fifo_seq0.sv) - класс sequence для генерации последовательности транзакций с только одной операцией (read, write или nop) и без возможности чтения из пустого fifo и записи в заполненный
    * [fifo_seq1.sv](./test_UVM/fifo_seq1.sv) - класс sequence для генерации последовательности транзакций с несколькими возможными операциями и возможностью записи в заполненный fifo или чтения из пустого
    * [fifo_transfer.sv](./test_UVM/fifo_transfer.sv) - объект, передаваемый от монитора в scoreboard. Содержит в себе сигналы, снятые по фронту
    * [simple_item.sv](./test_UVM/simple_item.sv) - объект, передаваемый от sequencer к драйверу
    * [simple_response.sv](./test_UVM/simple_response.sv) - объект, передаваемый от драйвера к sequencer
    * [test_lib.sv](./test_UVM/test_lib.sv) - содержит в себе два crt теста с seq0 и seq1
    * [top.sv](./test_UVM/top.sv) - верхний модуль проекта

## Пояснения к запуску

Для смены тестироваемого модуля ([fifo_cc_a](./RTL/fifo_cc_a.sv) или [fifo_cc_b](./RTL/fifo_cc_b.sv)) надо в файле [top.sv#L59](./test_UVM/top.sv#L59) изменить тип i_fifo_cc на нужный.
Чтобы выбрать запускаемый тест надо в [param.do#L40](./sim/param.do#L40) поменять test_name на нужный тест.
Для запуска проекта надо выполнить последовательность скриптов:
```
do clean.do
do compile.do
do sim.do
do wave.do
```
Более быстрый способ:
```
do rerun.do
```
Для включения логов надо в файлах [fifo_agent_cfg.sv](./test_UVM/fifo_agent_cfg.sv) и [fifo_env_cfg.sv](./test_UVM/fifo_env_cfg.sv) настроить объекты, в которых интересуют логи. Для включения покрытия - надо в [fifo_agent_cfg.sv#L20](./test_UVM/fifo_agent_cfg.sv#L20) поменять флаг покрытия и в [param.do#52](./sim/param.do#52) установить флаг в 1.
## Пояснения к проекту
Данный проект содержит в себе два варианта реализации FIFO с общим тактированием (на основе двухпортовой памяти и на основе сдвигого регистра). Для тестирования было реализовано два crt теста (передача только одной операции без проверки переполнения или переупостошения и передача до двух операций одновременно с проверкой переполнения и переопустошения)
